//========================== Load Modules Start ===========================

//========================== Load Internal Module =========================

// Load exceptions
import Exception from "./model/Exception.js"
import constants from "./constant.js"

//========================== Load Modules End =============================

//========================== Export Module Start ===========================

export default {
    intrnlSrvrErr: function (err) {
        return new Exception(1, constants.MESSAGES.INTERNAL_SERVER_ERROR, err);
    },
    unauthorizeAccess: function (err) {
        return new Exception(2, constants.MESSAGES.UNAUTHORIZED_ACCESS, err)
    },
    alreadyRegistered: function (err) {
        return new Exception(3, constants.MESSAGES.ALREADY_EXIST, err)
    },
    invalidEmail: function () {
        return new Exception(4, constants.MESSAGES.INVALID_EMAIL)
    },
    getCustomErrorException: function (errMsg, error) {
        return new Exception(5, errMsg, error);
    },
    userNotFound: function () {
        return new Exception(3, constants.MESSAGES.USER_NOT_REGISTERED)
    },
    incorrectPass: function () {
        return new Exception(3, constants.MESSAGES.INCORRECT_PASS)
    },
    otpNotValid: function () {
        return new Exception(3, constants.MESSAGES.WRONG_OTP)
    },
    emailNotVerified: function () {
        return new Exception(3, constants.MESSAGES.EMAIL_NOT_VERIFIED)
    },
    adminNotFound:  function () {
        return new Exception(3, constants.MESSAGES.ADMIN_NOT_EXIST)
    }
};

//========================== Export Module   End ===========================
