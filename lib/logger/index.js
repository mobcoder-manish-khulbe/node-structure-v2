import bunyan from 'bunyan'
import _ from 'lodash'
import path from 'path'
import * as dotenv from "dotenv"
dotenv.config()
//define _dirname
import { fileURLToPath } from 'url';
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);


var logger, streams;

const TAG = {
    REQUEST: 'Request',
    RESPONSE: 'Response',
    DEBUG: 'Debug',
    ERROR: 'Error',
    MESSAGE: 'Message'
}

import _debug from 'debug'
import bunyanDebugStream from 'bunyan-debug-stream'

const errLog = _debug(TAG.ERROR);

var initialize = _.once(function () {

    if (process.env.NODE_ENV === 'prod') {
        const stream = bunyanDebugStream.create({
            basepath: __dirname + '/..' // this should be the root folder of your project.
        })

        streams = [{
            level: 'error',
            type: 'raw',
            stream: stream
        }, {
            level: 'warn',
            type: 'raw',
            stream: stream
        }];
    } else {

        streams = [{
            level: 'error',
            path: 'app.log'
        }, {
            level: 'warn',
            path: 'app.log'
        }, {
            level: 'info',
            path: 'app.log'
        }];
    }

    logger = bunyan.createLogger({
        name: process.env.PROJECT_NAME,
        streams: streams,
        serializers: {
            req: bunyan.stdSerializers.req,
            res: bunyan.stdSerializers.res,
            err: bunyan.stdSerializers.err
        }
    });

});

initialize();

/**
 * Log Incoming Request Data
 * @param data
 * @param tag
 */
function log(tag, data) {
    //No Tag
    if (_.isEmpty(data)) {
        data = tag;
        tag = TAG.MESSAGE;
    }
    //No Array
    if (data.length == 1) {
        data = _.first(data); //Extract single element
    }
    //Print Error in case of Error
    if (data instanceof Error) {
        return errLog(data.stack);
    }
    //Log
    _debug(tag)(_getLoggableData(data));
}

/**
 * Logs Data only in debug mode
 * @param data
 */
function debug(data) {
    log(data, TAG.DEBUG);
}

/**
 * Get Loggable Version of Data to logged
 * @param data
  @returns {}
 * @private
 */
function _getLoggableData(data) {
    var to_log = _.clone(data);
    //Convert to String if Array
    to_log = to_log instanceof Array ? to_log.join('') : to_log;
    if (typeof to_log == 'string')
        to_log = to_log.replace('[time]', 'Time : ' + new Date());
    return to_log;
}

export default {
    logger
}