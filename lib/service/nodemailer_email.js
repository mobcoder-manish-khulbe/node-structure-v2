import config from '../config/index.js'
import nodemailer from "nodemailer"
import ejs from 'ejs'

const appName = config.cfg.app.app_name

function _templateRead(template, param) {


    let filename = "lib/emailTemplate/" + template;
    let data;

    ejs.renderFile(filename, param, function (err, htmlData) {

        if (htmlData) {
            data = htmlData
        }
        else {
            console.log(err)
            throw err
        }

    });

    return data

}

function sendEmail(payload) {

    payload.appName = appName
    var fromEmail = config.cfg.smtp.fromEmail;
    var toEmail = payload.email;
    var subject = payload.subject;
    var content = _templateRead(payload.template, payload);

    let smtpTransport = nodemailer.createTransport({
        service: config.cfg.smtp.fromEmail,
        port: 587,
        secure: false, // true for 465, false for other ports
        auth: {
            user: config.cfg.smtp.auth.user,
            pass: config.cfg.smtp.auth.pass,
        }
    });

    // setup email data with unicode symbols
    let mailOptions = {
        from: fromEmail, // sender address.  Must be the same as authenticated user if using Gmail.
        to: toEmail, // receiver
        subject: subject, // subject
        html: content // html body
    };


    return new Promise(function (resolve, reject) {
        smtpTransport.sendMail({ mailOptions }, function (err, data) {
            if (err) {
                return reject(err)
            }
            resolve(data);
        })
    })

}

// ========================== Export Module Start ==========================
export default {
    sendEmail
}
// ========================== Export Module End ============================