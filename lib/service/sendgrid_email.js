import config from '../config/index.js'
import ejs from 'ejs'
import sgMail from '@sendgrid/mail'

sgMail.setApiKey(config.cfg.sendgridKey);
const appName = config.cfg.app.app_name


async function _templateRead(template, params) {
    params.appName = appName
    let filename = "lib/emailTemplate/" + template;

    return await ejs.renderFile(filename, params)
        .then((result) => {
            return result
        })
        .catch((err) => {
            console.log(err)
            throw err
        })
}

async function sendEmail(payload) {
    const htmlContent = await _templateRead(payload.template, payload)

    const msg = {
        to: payload.email,
        from: config.cfg.smtp.fromEmail,
        subject: payload.subject,
        html: htmlContent,
    };
    if (payload.cc) {
        msg.cc = payload.cc;
    }
    if (payload.bcc) {
        msg.bcc = payload.bcc;
    }

    return await sgMail.send(msg)
        .then((result) => {
            console.log('Email sent sucessfully!')
            return result
        })
        .catch((err) => {
            console.log(err)
            throw err
        })
}



// ========================== Export Module Start ==========================
export default {
    sendEmail
}
// ========================== Export Module End ============================
