"use strict";
//========================== Load Modules Start ===========================
import mongoose from "mongoose";
//========================== Load internal Module =========================

import redisSessions from "../redisClient/session.js";
import customException from "../customException.js"
import constant from "../constant.js"

import userService from "../module/v1/user/userService.js"
//========================== Load Modules End =============================

var userRole = (role) => function (req, res, next) {
    console.log("role", role, req.user);
    let user = req.user;
    if (role) {
        if (role <= user.userType) {
            next();
        } else {
            throw customException.unauthorizeAccess();
        }
    } else {
        next();
    }
}


//========================== Export Module Start ===========================

export default {
    userRole
};

//========================== Export Module End ===========================
