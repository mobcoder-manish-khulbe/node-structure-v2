"use strict";
//========================== Load Modules Start ===========================
import requestIp from 'request-ip'
//========================== Load internal Module =========================
import whiteListService from "../module/whiteList/whiteListService.js"
//========================== Load Modules End =============================

var whitelistIP = function (request, response, next) {

    if (process.env.NODE_ENV === 'prod' || process.env.NODE_ENV === 'production') {
        return whiteListService.getAllwhiteListIP()
            .then(function (IPArr) {
                const clientIp = requestIp.getClientIp(request);
                console.log('Current IP ', clientIp);
                console.log('whitelist ', IPArr);

                let IPres = _.find(IPArr, function (IP) {
                    return IP.IP === clientIp;
                });
                if (IPres) {
                    next();
                } else {
                    response.statusCode = 401
                    response.send({ message: 'Access denied - Invalid IP' })
                }
            })
    } else {
        next();
    }
}


export default {
    whitelistIP
}
