"use strict";
//========================== Load Modules Start ===========================
import mongoose from "mongoose"
//========================== Load internal Module =========================

import redisSession from "../redisClient/session.js"
import customException from "../customException.js"

import userService from "../module/v1/user/userService.js"

//========================== Load Modules End =============================

var __verifyTok = async function (acsTokn) {
    try {
        return await redisSession.getByToken(acsTokn)
    }
    catch (err) {
        next(err)
    }
};

var expireToken = async function (req, res, next) {
    try {
        const acsToken = await req.get('accessToken');
        await redisSession.expire(acsToken)
        next()
    }
    catch (err) {
        next(err)
    }
}

var autntctTkn = async function (req, res, next) {
    try {

        const acsToken = await req.get("accessToken") ?? req.query.token;

        if (!acsToken) {
            return next(customException.unauthorizeAccess());
        }

        const tokenPayload = await __verifyTok(acsToken)

        if (tokenPayload.d) {
            let userId = mongoose.Types.ObjectId(tokenPayload.d.userId)
            req.user = tokenPayload.d;
            req.user.userId = userId;
            next()
        } else {
            throw customException.unauthorizeAccess();
        }
    }
    catch (err) {
        next(err)

    }
}

var autntctTknAdm = async function (req, res, next) {
    try {

        const acsToken = await req.get("accessToken") ?? req.query.token;

        if (!acsToken) {
            return next(customException.unauthorizeAccess());
        }

        const tokenPayload = await __verifyTok(acsToken)

        if (tokenPayload.d) {
            let userId = mongoose.Types.ObjectId(tokenPayload.d.userId)
            req.user = tokenPayload.d;
            req.user.userId = userId;
            next()
        } else {
            throw customException.unauthorizeAccess();
        }
    }
    catch (err) {
        next(err)

    }
}

var authSocketTkn = function (socket, next) {

    var accessToken = socket.handshake.query.accessToken;

    if (!accessToken) {
        return next(customException.unauthorizeAccess());
    }

    next();
    __verifyTok(accessToken)
        .bind({})
        .then(function (tokenPayload) {
            if (tokenPayload.d) {
                let paylod = tokenPayload.d;
                socket.payload = tokenPayload.d;

                return userService.getByKey({ _id: paylod.userId });

            } else {
                next()
            }
        })
        .then(function (user) {
            socket.user = user;
            next()
        })
        .catch(function (err) {
            next(new Error('Authentication error'));
        })
}


//========================== Export Module Start ===========================

export default {
    autntctTknAdm,
    autntctTkn,
    expireToken,
    authSocketTkn
};

//========================== Export Module End ===========================
