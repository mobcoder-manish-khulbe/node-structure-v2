import multer from './multer.js'
import authenticate from './authenticate.js'
import basicAuth from './basicAuth.js'
import mediaUpload from './mediaUpload.js'
import authenticateRole from './authenticateRole.js'

// ========================== Export Module Start ==========================
export default {
        multer,
        authenticate,
        basicAuth,
        mediaUpload,
        authenticateRole,
}
// ========================== Export Module End ============================