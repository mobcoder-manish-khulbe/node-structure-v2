import multer from 'multer'
import fs from 'fs'
import config from '../config/index.js'

// Set up Multer storage configuration
const storage = multer.diskStorage({
	destination: function (req, file, cb) {
		cb(null, 'uploads/'); // File will be saved in the "uploads" directory
	},
	filename: function (req, file, cb) {
		cb(null, file.originalname); // Use the original file name as the file name
	}
});

// Set up Multer upload middleware with the storage configuration
const upload = multer({ storage: storage });

function single(file) {
	return upload.single(file)
}

function multiple(files, size) {
	return upload.array(files, size)
}
// ========================== Export Module Start ==========================
export default {
	single,
	multiple
}
// ========================== Export Module End ============================