// Importing mongoose
import mongoose from "mongoose";
import constants from "../../constant.js";

var Schema = mongoose.Schema;


var whiteListSchema = new Schema({
    IP: {
        type: String,
        default: '',
        index: true
    },
    desc: {
        type: String,
    },
    status: {
        type: Number,
        default: 1
    },
    created: {
        type: Date,
        default: Date.now
    }
});

//Export whitelist module
export default mongoose.model(constants.DB_MODEL_REF.WHITELIST, whiteListSchema);

