"use strict";

//========================== Load Modules Start =======================

//========================== Load external modules ====================
// Load user service
import _ from "lodash"
//========================== Load internal modules ====================

import userService from './userService.js'
import userMapper from './userMapper.js'

import appUtils from '../../../appUtils.js'
import redisSession from "../../../redisClient/session.js"
import customException from "../../../customException.js"
import constants from '../../../constant.js'
import emailService from "../../../service/sendgrid_email.js"
import redis from '../../../redisClient/init.js'
import mail from '../../../service/nodemailer_email.js'

//========================== Load Modules End ==============================================

async function create(params) {
    try {

        //checking email is exist or not
        const isEmailExist = await userService.isEmailExist(params.email)

        //if email exist but not verified by otp, simpply delete the data from DB so that you can again sned the otp for verification
        if (isEmailExist && isEmailExist.status == 1) {
            userService.deleteUser(isEmailExist.email)
        }

        //if email is exist and user is verified, user will not able to again signup with the same email
        if (isEmailExist && isEmailExist.status == 2) {
            throw customException.alreadyRegistered();
        }

        //now firstly store the user data in Db
        const createUser = await userService.createUser(params)

        if (createUser) {

            //now create the emailObj to sending the email for otp
            const emailObj = {
                template: constants.EMAIL.TEMPLATE.OTP,
                email: createUser.email,
                subject: constants.EMAIL.SUBJECT.VERIFY_EMAIL,
                name: createUser.name,
                otp: createUser.otp
            }

            //store the otp in redis
            await redis.setValue(createUser.email, createUser.otp)

            //sneding the mail
            await emailService.sendEmail(emailObj)

            //return the response
            return userMapper.createMapping(createUser);
        }
    }
    catch (err) {
        //throw error if error occurs
        return err
    }
}

async function verifyEmail(params) {

    try {
        //checking email is exist or not
        const isEmailExist = await userService.isEmailExist(params.email)

        if (!isEmailExist) {
            throw customException.userNotFound()
        }

        //if email is exist and user is verified, user will not able to again signup with the same email
        if (isEmailExist && isEmailExist.status == 2) {
            throw customException.alreadyRegistered();
        }

        const otp = await redis.getValue(params.email)

        let isOtpValid;

        if (params.otp == otp) {
            isOtpValid = true
        }
        else {
            isOtpValid = false
        }

        if (isOtpValid) {

            const user = await userService.verifyEmail(params)

            const accessTokenObj = {
                ip: params.clientIp,
                userId: isEmailExist._id,
                userObj: _buildUserTokenGenObj(isEmailExist)
            }

            const { token } = await redisSession.create(accessTokenObj)

            return userMapper.loginMapping(user, token)
        }
        else {
            throw customException.otpNotValid()
        }
    }
    catch (err) {
        throw err
    }
}

async function otpResend(email) {

    try {
        const isEmailExist = await userService.isEmailExist(email)

        if (isEmailExist && isEmailExist.status == 2) {
            throw customException.alreadyRegistered();
        }

        const otp = userService.generateOtp()

        //now create the emailObj to sending the email for otp
        const emailObj = {
            template: constants.EMAIL.TEMPLATE.OTP,
            email: isEmailExist.email,
            subject: constants.EMAIL.SUBJECT.VERIFY_EMAIL,
            name: isEmailExist.name,
            otp: otp
        }

        //store the otp in redis
        await redis.setValue(email, otp)

        //sneding the mail
        // await emailService.sendEmail(emailObj)
        await mail.sendEmail(emailObj)

        //return the response
        return userMapper.otpResend(email);
    }
    catch (err) {
        return err
    }
}

async function userLogin(params) {
    try {
        const userDetails = await userService.isEmailExist(params.email)

        if (!userDetails) {
            throw customException.incorrectPass()
        }

        if (userDetails && userDetails.status == 1) {
            throw customException.emailNotVerified()
        }

        if (!(appUtils.compareHashPassword(params.password, userDetails.password))) {
            throw customException.incorrectPass()
        }

        const accessTokenObj = {
            ip: params.clientIp,
            userId: userDetails._id,
            userObj: _buildUserTokenGenObj(userDetails)
        }

        const { token } = await redisSession.create(accessTokenObj)

        return userMapper.loginMapping(userDetails, token)
    }
    catch (err) {
        return err
    }
}

async function forgetPassword(params) {

    try {
        const userDetails = await userService.isEmailExist(params.email)

        if (!userDetails) {
            throw customException.userNotFound()
        }

        if (userDetails && userDetails.status == 1) {
            throw customException.emailNotVerified()
        }

        const otp = userService.generateOtp()

        //now create the emailObj to sending the email for otp
        const emailObj = {
            template: constants.EMAIL.TEMPLATE.OTP,
            email: userDetails.email,
            subject: constants.EMAIL.SUBJECT.VERIFY_EMAIL,
            name: userDetails.name,
            otp: otp
        }

        //store the otp in redis
        await redis.setValue(userDetails.email, otp)

        //sneding the mail
        await emailService.sendEmail(emailObj)

        //return the response
        return userMapper.otpResend(userDetails.email);
    }
    catch (err) {
        return err
    }
}

async function resetPassword(params) {

    try {
        const otp = await redis.getValue(params.email)

        let isOtpValid;

        if (params.otp == otp) {
            isOtpValid = true
        }
        else {
            isOtpValid = false
        }


        if (isOtpValid) {

            const user = await userService.changePassword(params)

            return userMapper.changePassword(user)
        }
        else {
            throw customException.otpNotValid()
        }
    }
    catch (err) {
        return err
    }
}

async function logout() {
    return userMapper.logout()
}

async function deleteAccount(email) {
    try {
        await userService.deleteAccount(email)

        return userMapper.deleteAccount()
    }
    catch (err) {
        return err
    }
}

async function socialSignup(params) {
    try {
        const isSocialIdExist = await userService.isSocialIdExist(params.id)

        // const isEmailExist = await userService.isEmailExist(params.email)

        if (isEmailExist) {
            throw customException.alreadyRegistered();
        }

        const socialSignup = await userService.socialSignup(params)

        return userMapper.socialSignup(socialSignup)
    }
    catch (err) {
        return err
    }
}

function _buildUserTokenGenObj(user) {
    var userObj = {};
    const role = 'user'
    userObj.role = role
    userObj.deviceToken = user.deviceToken ? user.deviceToken : null;
    userObj.deviceTypeID = user.deviceTypeID ? user.deviceTypeID : null;
    userObj.deviceID = user.deviceID ? user.deviceID : null;
    userObj.userId = user._id.toString();
    userObj.userType = user.userType;
    userObj.status = user.status;
    return userObj;
}


//========================== Export Module Start ==============================

export default {
    userLogin,
    create,
    verifyEmail,
    otpResend,
    forgetPassword,
    resetPassword,
    logout,
    deleteAccount,
    socialSignup
};

//========================== Export Module End ================================