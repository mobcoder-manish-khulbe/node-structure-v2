"use strict";

//========================== Load Modules Start =======================
import appUtils from "../../../appUtils.js";
//========================== Load internal modules ====================
import userDao from './userDao.js'

//========================== Load Modules End ==============================================

function isEmailExist(email) {
    return userDao.isEmailExist(email)
}

async function createUser(params) {
    params.password = appUtils.encryptHashPassword(params.password) //change plain password to hash password
    const userDetails = await userDao.createUser(params)
    userDetails.otp = appUtils.getRandomOtp()                       //add random otp to userdetails
    return userDetails;
}

async function deleteUser(email) {
    return await userDao.deleteUser(email)
}

async function verifyEmail(params) {
    return await userDao.verifyEmail(params)
}

function generateOtp() {
    return appUtils.getRandomOtp()

}

async function changePassword(params) {
    params.password = appUtils.encryptHashPassword(params.password) //change plain password to hash password
    return await userDao.changePassword(params)
}

async function deleteAccount(email) {
    return userDao.deleteUser(email)
}

async function isSocialIdExist(id) {
    return await userDao.isSocialIdExist(id)
}

async function socialSignup(params) {
    return await userDao.socialSignup(params)
}

function getByKey(param) {
    return userDao.getByKey(param)
}


//========================== Export Module Start ==============================

export default {
    verifyEmail,
    socialSignup,
    deleteAccount,
    generateOtp,
    changePassword,
    createUser,
    isEmailExist,
    getByKey,
    deleteUser,
    isSocialIdExist
};

//========================== Export Module End ===============================
