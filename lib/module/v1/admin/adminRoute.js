import express from "express"
var adminRouter = express.Router();
import requestIp from 'request-ip'

import resHndlr from "../../../responseHandler.js"
import middleware from "../../../middleware/index.js"
import adminFacade from "./adminFacade.js"
import validators from "./adminValidators.js"

//==============================================================

adminRouter.route("/create")
    .post(middleware.multer.single('profileImage'), middleware.mediaUpload.uploadSingleMediaToS3('profile'), async function (req, res) {
        try {
            let {
                email,
                password,
                adminType,
            } = req.body;

            let clientIp = requestIp.getClientIp(req);

            const result = await adminFacade.create({
                email,
                password,
                adminType,
                clientIp
            })

            resHndlr.sendSuccess(res, result, req);
        }
        catch (err) {
            resHndlr.sendError(res, err, req);
        }
    })

adminRouter.route("/login")
    .post(async function (req, res) {
        try {

            let { email, password } = req.body;

            let clientIp = requestIp.getClientIp(req);

            const result = await adminFacade.adminLogin({ email, password, clientIp })

            resHndlr.sendSuccess(res, result, req);
        }
        catch (err) {
            resHndlr.sendError(res, err, req);
        }
    });

adminRouter.route("/logout")
    .post([middleware.authenticate.autntctTkn], [middleware.authenticate.expireToken], async function (req, res) {
        try {

            const result = await adminFacade.logout()

            resHndlr.sendSuccess(res, result, req);
        }
        catch (err) {
            resHndlr.sendError(res, err, req);
        }
    });

adminRouter.route("/deleteAccount")
    .delete([middleware.authenticate.autntctTkn], [middleware.authenticate.expireToken], async function (req, res) {
        try {
            const { email } = req.query

            const result = await adminFacade.deleteAccount(email)

            resHndlr.sendSuccess(res, result, req);
        }
        catch (err) {
            resHndlr.sendError(res, err, req);
        }
    });

//resetPassword
adminRouter.route("/resetPassword")
    .delete([middleware.authenticate.autntctTkn], [middleware.authenticate.expireToken], async function (req, res) {
        try {
            const { email } = req.query

            const result = await adminFacade.deleteAccount(email)

            resHndlr.sendSuccess(res, result, req);
        }
        catch (err) {
            resHndlr.sendError(res, err, req);
        }
    });


export default adminRouter;
