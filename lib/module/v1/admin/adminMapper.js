/**
 * This file will have request and response object mappings.
 */
import _ from "lodash"
import contstants from "../../../constant.js"

function createMapping(params) {

    let respObj = {
        email: params.email,
        adminType: params.adminType,
    };

    return {
        "message": contstants.MESSAGES.ADMIN_CREATED,
        'result': respObj
    }
}

function loginMapping(params, token) {
    return {
        message: contstants.MESSAGES.USER_LOGIN,
        token: token,
        email: params.email
    }

}

function changePassword(params) {
    return {
        message: contstants.MESSAGES.PASSWORD_CHANGED,
        email: params.email
    }
}

function logout() {
    return {
        message: contstants.MESSAGES.LOGOUT
    }
}

function deleteAccount() {

    return {
        message: contstants.MESSAGES.ACCOUNT_DELETED
    }
}

export default {
    logout,
    loginMapping,
    deleteAccount,
    createMapping,
    changePassword,
}