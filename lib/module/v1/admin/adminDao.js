"use strict";
//========================== Load Modules Start =======================

//========================== Load internal modules ====================
import mongoose from "mongoose";
import _ from 'lodash'
//========================== Load internal modules ====================
import adminModel from './adminModel.js'

// init admin dao
import BaseDao from '../../../dao/baseDao.js'
const adminDao = new BaseDao(adminModel);


//========================== Load Modules End ==============================================

async function isEmailExist(email) {
    try {
        return await adminDao.findOne({ email: email })
    }
    catch (err) {
        return err
    }
}

async function createAdmin(params) {
    let query = {};

    query.name = params.name,
        query.email = params.email,
        query.password = params.password,
        query.adminname = params.adminname,
        query.adminType = params.adminType,
        query.gender = params.gender,
        query.dob = params.dob,
        query.designation = params.designation,
        query.companyName = params.companyName,
        query.phoneNo = params.phoneNo,
        query.clientIp = params.clientIp

    return await adminDao.save(query)
}

async function deleteadmin(email) {
    return await adminDao.deleteOne({ email: email })
}

async function changePassword(params) {
    return await adminDao.findOneAndUpdate({ email: params.email }, { password: params.password })
}

function getByKey(query) {
    return adminDao.findOne(query)
}

//========================== Export Module Start ==============================

export default {
    createAdmin,
    changePassword,
    getByKey,
    isEmailExist,
    deleteadmin
};

//========================== Export Module End ===============================
