// Importing mongoose
import mongoose from "mongoose";
import constants from "../../../constant.js";

var Schema = mongoose.Schema;
var Admin;

var AdminSchema = new Schema({
    email: {
        type: String,
        index: true,
        unique: true,
    },
    password: {
        type: String
    },
    adminType: {
        type: Number,
        default: 1, //1-superAdmin, 2-subAdmin
    },
    status: {
        type: Number, // 1: Active, 2:In-active
        default: 1
    },
    deleteInfo: {
        isDelete: {
            type: Boolean,
            default: false,
        },
        deleteDate: {
            type: Date,
        },
    }
},
    {
        timestamps: true
    }
);

//Export user module
Admin = mongoose.model(constants.DB_MODEL_REF.ADMIN, AdminSchema);

export default Admin


