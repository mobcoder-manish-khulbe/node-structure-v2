"use strict"
//=================================== Load Modules start ===================================

//=================================== Load external modules=================================
import mongoose from "mongoose";
//Import logger
import loggerh from "../logger/index.js";
const logger = loggerh.logger
import appUtils from "../appUtils.js"
import adminService from "../module/v1/admin/adminService.js"
import whiteListService from "../module/whiteList/whiteListService.js"
import config from "./index.js"

//=================================== Load Modules end =====================================

//set strictQuery false for mongoose
mongoose.set('strictQuery', false)

// Connect to Db
async function connectDb(env, callback) {

    try {
        let dbName = env.mongo.dbName;
        let dbUrl = env.mongo.dbUrl;
        let dbOptions = env.mongo.options;


        if (env.debug === true || env.debug === "true") {
            logger.info("Configuring db in " + env.TAG + ' mode');
            dbUrl = dbUrl + dbName;
            mongoose.set('debug', true);
        } else {
            logger.info("Configuring db in " + env.TAG + ' mode');
            dbUrl = dbUrl + dbName;
        }

        mongoose.connect(dbUrl, dbOptions);
        logger.info("Connecting to -> " + dbUrl);

        // CONNECTION EVENTS
        // When successfully connected
        mongoose.connection.on('connected', function () {
            logger.info('Connected to DB', dbName, 'at', dbUrl);
            callback();
        });

        // If the connection throws an error
        mongoose.connection.on('error', function (error) {
            logger.info('DB connection error: ' + error);
            callback(error);
        });

        // When the connection is disconnected
        mongoose.connection.on('disconnected', function () {
            logger.info('DB connection disconnected.');
            callback("DB connection disconnected.");
        });

        // project run admin create
        mongoose.connection.on('open', function () {

            let param = {};
            param.email = config.cfg.adminEmail;
            param.password = appUtils.createHashSHA256(config.cfg.adminPassword);
            param.status = 1;
            param.userType = 1;
            adminService.createAdmin(param)

            whiteListService.createIP({ IP: '::1' })
        });
    }
    catch (err) {
        throw err
    }
}

// ========================== Export Module Start ==========================
export default { connectDb };
// ========================== Export Module End ============================