import express from 'express'
import responseTime from 'response-time';

import swagger from '../../swagger/swaggerSetup.js'
export default function (app, env) {

    // parses application/json bodies
    app.use(express.json());

    // // parses application/x-www-form-urlencoded bodies
    // // use queryString lib to parse urlencoded bodies
    app.use(express.urlencoded({ extended: true }));

    app.use(responseTime());

    /**
     * add swagger to our project
     */
    swagger(app)

    app.use('/chatTest', express.static(app.locals.rootDir + '/views'));

    app.use('/download', express.static(app.locals.rootDir + '/download'));

    app.use('/urltest', express.static(app.locals.rootDir + '/public/urltest'));

    app.use('/socket-admin', express.static(app.locals.rootDir + '/public/socketAdmin'));

    /*
     * all api request
     */
    app.use(function (request, response, next) {
        response.header("Access-Control-Allow-Origin", "*");
        response.header('Access-Control-Allow-Credentials', true);
        response.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, authorization, accessToken");
        response.setHeader('Access-Control-Allow-Methods', 'POST, GET, PUT, DELETE, OPTIONS');
        next();
    });
}