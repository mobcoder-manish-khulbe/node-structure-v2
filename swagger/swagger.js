const swagger = {
  "swagger": "2.0",
  "info": {
    "title": "update",
    "description": "This is a sample server Petstore server.  You can find out more about Swagger at [http://swagger.io](http://swagger.io) or on [irc.freenode.net, #swagger](http://swagger.io/irc/).  For this sample, you can use the api key `special-key` to test the authorization filters.",
    "contact": {
      "email": ""
    },
    "version": "1.0.0",
    "termsOfService": "http://swagger.io/terms/",
    "license": {
      "name": "Apache 2.0",
      "url": "http://www.apache.org/licenses/LICENSE-2.0.html"
    }
  },
  "basePath": "/api/v1",
  "tags": [
    {
      "name": "User",
      "description": "Operations about user",
      "externalDocs": {
        "description": "Find out more about our store",
        "url": ""
      }
    },
    {
      "name": "Admin",
      "description": "Operations about user",
      "externalDocs": {
        "description": "Find out more about our store",
        "url": ""
      }
    }
  ],
  "schemes": [
    "http",
    "https"
  ],
  "consumes": [
    "application/json",
    "application/x-www-form-urlencoded",
    "multipart/form-data"
  ],
  "produces": [
    "application/json"
  ],
  "paths": {
    "/user/signup": {
      "post": {
        "deprecated": false,
        "tags": [
          "User"
        ],
        "description": "User Signup",
        "consumes": [
          "multipart/form-data"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "name",
            "in": "formData",
            "description": "name",
            "required": true,
            "type": "string"
          },
          {
            "name": "email",
            "in": "formData",
            "description": "email",
            "required": true,
            "type": "string"
          },
          {
            "name": "password",
            "in": "formData",
            "description": "password",
            "required": true,
            "type": "string"
          },
          {
            "name": "username",
            "in": "formData",
            "description": "username",
            "required": false,
            "type": "string"
          },
          {
            "name": "userType",
            "in": "formData",
            "description": "1-user, 2-company",
            "required": false,
            "enum": [
              1,
              2
            ],
            "type": "integer"
          },
          {
            "name": "gender",
            "in": "formData",
            "description": "gender (1 Male, 2 Female, 3 Others)",
            "required": false,
            "enum": [
              1,
              2,
              3
            ],
            "type": "integer"
          },
          {
            "name": "dob",
            "in": "formData",
            "description": "dob",
            "required": false,
            "type": "string"
          },
          {
            "name": "designation",
            "in": "formData",
            "description": "designation",
            "required": false,
            "type": "string"
          },
          {
            "name": "companyName",
            "in": "formData",
            "description": "companyName",
            "required": false,
            "type": "string"
          },
          {
            "name": "phoneNo",
            "in": "formData",
            "description": "phoneNo",
            "required": false,
            "type": "string"
          },
          {
            "name": "profileImage",
            "in": "formData",
            "description": "Profile Picture",
            "required": false,
            "type": "file"
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully"
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "security": [
          {
            "basicAuth": [
              "Username: ",
              "Password: "
            ]
          }
        ]
      }
    },
    "/user/verifyEmail": {
      "put": {
        "deprecated": false,
        "tags": [
          "User"
        ],
        "description": "Verify Email",
        "consumes": [
          "application/x-www-form-urlencoded"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "email",
            "in": "query",
            "description": "Email",
            "required": true,
            "type": "string"
          },
          {
            "name": "otp",
            "in": "query",
            "description": "OTP",
            "required": true,
            "type": "integer"
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully"
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "security": [
          {
            "basicAuth": [
              "Username: ",
              "Password: "
            ]
          }
        ]
      }
    },
    "/user/resendOtp": {
      "post": {
        "deprecated": false,
        "tags": [
          "User"
        ],
        "description": "resend otp ",
        "consumes": [
          "application/x-www-form-urlencoded"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "email",
            "in": "formData",
            "description": "Email",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully"
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "security": [
          {
            "basicAuth": [
              "Username: ",
              "Password: "
            ]
          }
        ]
      }
    },
    "/user/login": {
      "post": {
        "deprecated": false,
        "tags": [
          "User"
        ],
        "description": "Login",
        "consumes": [
          "application/x-www-form-urlencoded"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "email",
            "in": "formData",
            "description": "email",
            "required": true,
            "type": "string"
          },
          {
            "name": "password",
            "in": "formData",
            "description": "password",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully"
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "security": [
          {
            "basicAuth": [
              "Username: ",
              "Password: "
            ]
          }
        ]
      }
    },
    "/user/socialLogin": {
      "get": {
        "deprecated": false,
        "tags": [
          "User"
        ],
        "description": "Login",
        "consumes": [
          "application/x-www-form-urlencoded"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "clientId",
            "in": "formData",
            "description": "email",
            "required": true,
            "type": "string"
          },
          {
            "name": "password",
            "in": "formData",
            "description": "password",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully"
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "security": [
          {
            "basicAuth": [
              "Username: ",
              "Password: "
            ]
          }
        ]
      }
    },
    "/user/forgot": {
      "post": {
        "deprecated": false,
        "tags": [
          "User"
        ],
        "description": "User forgot",
        "consumes": [
          "application/x-www-form-urlencoded"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "email",
            "in": "formData",
            "description": "email",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully"
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "security": [
          {
            "basicAuth": [
              "Username: ",
              "Password: "
            ]
          }
        ]
      }
    },
    "/user/reset": {
      "post": {
        "deprecated": false,
        "tags": [
          "User"
        ],
        "description": "User reset password",
        "consumes": [
          "application/x-www-form-urlencoded"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "email",
            "in": "formData",
            "description": "email",
            "required": true,
            "type": "string"
          },
          {
            "name": "otp",
            "in": "formData",
            "description": "otp",
            "required": true,
            "type": "integer"
          },
          {
            "name": "password",
            "in": "formData",
            "description": "password",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully"
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "security": [
          {
            "basicAuth": [
              "Username: ",
              "Password: "
            ]
          }
        ]
      }
    },
    "/user/logout": {
      "post": {
        "deprecated": false,
        "tags": [
          "User"
        ],
        "description": "Logout",
        "consumes": [
          "application/x-www-form-urlencoded"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
        ],
        "responses": {
          "200": {
            "description": "Successfully"
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "security": [
          {
            "basicAuth": [
              "Username: ",
              "Password: "
            ]
          },
          {
            "bearerAuth": []
          }
        ]
      }
    },
    "/user/deleteAccount": {
      "delete": {
        "deprecated": false,
        "tags": [
          "User"
        ],
        "description": "Delete Account",
        "consumes": [
          "application/x-www-form-urlencoded"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "email",
            "in": "query",
            "description": "Email",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully"
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "security": [
          {
            "basicAuth": [
              "Username: ",
              "Password: "
            ]
          },
          {
            "bearerAuth": []
          }
        ]
      }
    },
    "/admin/create": {
      "post": {
        "deprecated": false,
        "tags": [
          "Admin"
        ],
        "description": "User Signup",
        "consumes": [
          "multipart/form-data"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "email",
            "in": "formData",
            "description": "email",
            "required": true,
            "type": "string"
          },
          {
            "name": "password",
            "in": "formData",
            "description": "password",
            "required": true,
            "type": "string"
          },
          {
            "name": "adminType",
            "in": "formData",
            "description": "1-SuperAdmin, 2-Sub-Admin",
            "required": true,
            "enum": [
              1,
              2
            ],
            "type": "integer"
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully"
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "security": [
          {
            "basicAuth": [
              "Username: ",
              "Password: "
            ]
          }
        ]
      }
    },
    "/admin/login": {
      "post": {
        "deprecated": false,
        "tags": [
          "Admin"
        ],
        "description": "Login",
        "consumes": [
          "application/x-www-form-urlencoded"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "email",
            "in": "formData",
            "description": "email",
            "required": true,
            "type": "string"
          },
          {
            "name": "password",
            "in": "formData",
            "description": "password",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully"
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "security": [
          {
            "basicAuth": [
              "Username: ",
              "Password: "
            ]
          }
        ]
      }
    },
    "/admin/logout": {
      "post": {
        "deprecated": false,
        "tags": [
          "Admin"
        ],
        "description": "Logout",
        "consumes": [
          "application/x-www-form-urlencoded"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
        ],
        "responses": {
          "200": {
            "description": "Successfully"
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "security": [
          {
            "basicAuth": [
              "Username: ",
              "Password: "
            ]
          },
          {
            "bearerAuth": []
          }
        ]
      }
    },
    "/admin/deleteAccount": {
      "delete": {
        "deprecated": false,
        "tags": [
          "Admin"
        ],
        "description": "Delete Account",
        "consumes": [
          "application/x-www-form-urlencoded"
        ],
        "produces": [
          "application/json"
        ],
        "parameters": [
          {
            "name": "email",
            "in": "query",
            "description": "Email",
            "required": true,
            "type": "string"
          }
        ],
        "responses": {
          "200": {
            "description": "Successfully"
          },
          "401": {
            "description": "Unauthorized"
          }
        },
        "security": [
          {
            "basicAuth": [
              "Username: ",
              "Password: "
            ]
          },
          {
            "bearerAuth": []
          }
        ]
      }
    }
  },
  "securityDefinitions": {
    "basicAuth": {
      "type": "basic"
    },
    "bearerAuth": {
      "type": "apiKey",
      "name": "accesstoken",
      "scheme": "bearer",
      "in": "header",
      "description": "JWT Authorization header"
    }
  }
}

export default swagger